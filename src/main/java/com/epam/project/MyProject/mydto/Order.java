package com.epam.project.MyProject.mydto;

import java.time.LocalDate;

public class Order {

    private int id;
    private int id_users;
    private String order;
    private int id_tour;
    private int price;
    private LocalDate date;
    private String username;
    private String name;
    private int firetour;
    private int discount;

    public Order() { };

    public Order(String username,  String name, LocalDate date, int price, int discount) {
        this.username = username;
        this.name = name;
        this.date = date;
        this.price = price;
        this.discount = discount;
    }

    public Order(int id, int id_users, String order, int id_tour, int price, LocalDate date, int firetour, int discount) {
        this.id = id;
        this.id_users = id_users;
        this.order = order;
        this.id_tour = id_tour;
        this.price = price;
        this.date = date;
        this.firetour = firetour;
        this.discount = discount;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getFiretour() {
        return firetour;
    }

    public void setFiretour(int firetour) {
        this.firetour = firetour;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_users() {
        return id_users;
    }

    public void setId_users(int id_users) {
        this.id_users = id_users;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getId_tour() {
        return id_tour;
    }

    public void setId_tour(int id_tour) {
        this.id_tour = id_tour;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}

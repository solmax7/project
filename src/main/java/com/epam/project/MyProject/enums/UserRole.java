package com.epam.project.MyProject.enums;

public enum UserRole {

    ADMINISTRATOR, 
    USER;
}

package com.epam.project.MyProject.interceptor;

import com.epam.project.MyProject.mydto.User;
import com.epam.project.MyProject.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserNameAwareInterceptor implements HandlerInterceptor {

    @Autowired
    private UserManager userManager;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
//        User currentUser = userManager.getUsers().get(0);
//
//        modelAndView.addObject("userName", currentUser.getLogin());
       // modelAndView.addObject("currentUserRole", currentUser.getRole());
    }

}

package com.epam.project.MyProject.repository;

import com.epam.project.MyProject.exception.UnauthorizedException;
import com.epam.project.MyProject.mapper.toursRowMapper;
import com.epam.project.MyProject.mapper.usersRowMapper;
import com.epam.project.MyProject.mydto.Tour;
import com.epam.project.MyProject.mydto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class tourSql {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    toursRowMapper Row;

    public List<Tour> getByTours() {
        String sql = "SELECT * FROM tour";
        return  jdbcTemplate.query(sql, Row::mapRow);
    }
    public List<Tour> getByToursName(String name) {
        String sql = "SELECT * FROM tour where tour.name = ?";
        return  jdbcTemplate.query(sql, new Object[]{name}, Row::mapRow);
    }

    public void addTour(Tour tour) {
        String sql = "INSERT INTO tour (name, description, price, date) " +
                "VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(sql, tour.getName(), tour.getDescription(), tour.getPrice(), tour.getData());
    }

    public List<Tour> getByTourByName(String name) {
        String sql = "SELECT * FROM tour where tour.name = ?";
        return jdbcTemplate.query(sql, new Object[]{name}, Row::mapRow);
    }


    public List<Tour> getByTourById(int id) {
        String sql = "SELECT * FROM tour where tour.id = ?";
        return jdbcTemplate.query(sql, new Object[]{id}, Row::mapRow);
    }

    public List<Tour> FindUser(User user) throws UnauthorizedException {

        String sql = "SELECT * FROM user where user.login = ? and user.password = ?";

        return jdbcTemplate.query(sql, new Object[]{user.getLogin(), user.getPassword()}, Row::mapRow);

    }



}

package com.epam.project.MyProject.service;

import com.epam.project.MyProject.mydto.Order;
import com.epam.project.MyProject.repository.orderSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    orderSql orderQuery;

    @Override
    public void addOrder(int tour, int user, int price, LocalDate date, int firetour, int discount) {

        orderQuery.addOrder(tour, user, price, date, firetour, discount);

    }

    @Override
    public List<Order> ShowAllOrders() {

        List <Order> orders = orderQuery.getByOrders();

        return orders;
    }

    @Override
    public List<Order> ShowAllOrdersUsers(int id) {

        List <Order> orders = orderQuery.getByUserOrders(id);

        return orders;
    }


}

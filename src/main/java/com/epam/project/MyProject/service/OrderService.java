package com.epam.project.MyProject.service;

import com.epam.project.MyProject.mydto.Order;
import com.epam.project.MyProject.mydto.Tour;

import java.time.LocalDate;
import java.util.List;

public interface OrderService {

       void addOrder(int tour, int user, int price, LocalDate date, int firetour, int discount);

       List<Order> ShowAllOrders();

      List<Order> ShowAllOrdersUsers(int id);

}

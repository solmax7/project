package com.epam.project.MyProject.controller;


import com.epam.project.MyProject.exception.UnauthorizedException;
import com.epam.project.MyProject.exception.UserAlreadyExistException;
import com.epam.project.MyProject.mydto.User;
import com.epam.project.MyProject.service.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);


    @Autowired
    private UserManager userManager;

    @GetMapping("/login")
    public ModelAndView getLoginPage(ModelAndView modelAndView) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("login-page");

        return modelAndView;
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login-page");

        request.getSession().invalidate();

        return modelAndView;
    }

    @PostMapping("/login")
    public ModelAndView login(User user) throws  UnauthorizedException {
        ModelAndView modelAndView = new ModelAndView();

        List<User> foundUser;

        foundUser = userManager.FindUser(user);

        if  (foundUser.size() == 0) {

            //} catch (UserAlreadyExistException e) {
            modelAndView.addObject("errorMsg", "Неверный логин или пароль");
            modelAndView.setViewName("login-page");
        }
        else {

            if (foundUser.get(0).getRole()){

                modelAndView.addObject("currentUser", foundUser.get(0));
                userManager.setUser(foundUser.get(0));
                modelAndView.setViewName("redirect:/admin");
            }

            else {

                modelAndView.addObject("currentUser", foundUser.get(0));
                userManager.setUser(foundUser.get(0));
                modelAndView.setViewName("redirect:/home");
            }
        }

        return modelAndView;

    }

    @GetMapping("/registration")
    public ModelAndView registrationPage(User user) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("registration");

        return modelAndView;

    }

    @PostMapping("/registration")
    public ModelAndView registration(@Validated User user, BindingResult result) throws UserAlreadyExistException {
        ModelAndView modelAndView = new ModelAndView();

          if  (userManager.GetUserByLogin(user.getLogin()).size() !=0) {

            modelAndView.addObject("errorMsg", "Такой пользователь уже существует");
            modelAndView.setViewName("registration");
        }
        else {

            userManager.addUserRegistration(user);
            modelAndView.setViewName("redirect:/login");
          }

        if (result.hasErrors()) {
            modelAndView.addObject("errors", result.getAllErrors());
        }

        return modelAndView;
    }

}

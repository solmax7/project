<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Авторизация</title>
	<link rel="stylesheet" href="css/login.css" media="screen" type="text/css" />

</head>
<body>
 <div id="login">
  <div class="flip">

    <div id ="form-login">
      <h1>Авторизация</h1>
      <fieldset>
        <form action="/login" method="post" id="form-login">
          <input id="user" name="login" type="text" placeholder="Логин или Email" required />
          <input type="password" name="password" placeholder="Пароль" required />
          <input type="submit" value="ВОЙТИ" />
          ${errorMsg}
        </form>

        <p><a href="/registration" class="flipper">Регистрация.</a><br>
       </p>
      </fieldset>
    </div>
  </div>
</div>
</div>
	<div class="footer"> &copy;Туристическая компания "TOURS" &nbsp;<span class="separator">|</span>&nbsp; <a href="http://tours.ru">TOURS</a> </div>
	</div>
</body>
<script language="JavaScript" type="text/javascript" src="css/js/jquery-3.3.1.min.js"></script>
<script language="JavaScript" type="text/javascript" src="css/js/login.js"></script>
</html>
package com.epam.project.MyProject.controller;

import com.epam.project.MyProject.mydto.Tour;
import com.epam.project.MyProject.mydto.User;
import com.epam.project.MyProject.service.TourService;
import com.epam.project.MyProject.service.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private UserManager sessionUserManager;

    @Autowired
    private TourService tourManager;

    @RequestMapping("/home")
    public ModelAndView hello(HttpServletRequest request){

        ModelAndView modelAndView = new ModelAndView();
        User currentUser = sessionUserManager.getUser();

        if (currentUser.getRole()){

            modelAndView.addObject("currentUser", currentUser);
            modelAndView.setViewName("redirect:/admin");

        }

        else {

            modelAndView.addObject("currentUser", currentUser);
            modelAndView.setViewName("index");
        }

        List<Tour> tour = tourManager.ShowAllTours();

        modelAndView.addObject("tour", tour);
        return modelAndView;
    }


    @GetMapping("/admin")
    private ModelAndView admin(ModelAndView modelAndView) {


        modelAndView.setViewName("admin-page");
        return new ModelAndView("admin-page", "employee", new Tour());
    }

    @RequestMapping("/about")
    public ModelAndView about(HttpServletRequest request){

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("about");
        return modelAndView;
    }

}

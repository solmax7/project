package com.epam.project.MyProject.mapper;

import com.epam.project.MyProject.mydto.Order;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class orderRowMapper implements RowMapper {

    public Order mapRow(ResultSet resultSet, int i) throws SQLException {
        Order order = new Order(resultSet.getInt("id"),
                resultSet.getInt("id_users"), resultSet.getString("order"), resultSet.getInt("id_tour"), resultSet.getInt("price"), resultSet.getDate("date").toLocalDate(), resultSet.getInt("firetour"), resultSet.getInt("discount"));
        return order;
    }

    public Order mapRowUser(ResultSet resultSet, int i) throws SQLException {
        Order order = new Order(resultSet.getString("username"),
                resultSet.getString("name"), resultSet.getDate("date").toLocalDate(), resultSet.getInt("price"), resultSet.getInt("discount"));
        return order;
    }
}

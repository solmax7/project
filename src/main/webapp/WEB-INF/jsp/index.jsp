<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>

<head>

     <meta charset="UTF-8">
     <title>Турагенство</title>
     <link rel="stylesheet" href="css/style.css" media="screen" type="text/css" />
     <link rel="stylesheet" href="css/index.css" media="screen" type="text/css" />
</head>

<body>

<!----
     <header style="background-color:gray; height:25px">
          <div><a href="/logout" style="margin-left: 97%; margin-top: 100px;">Выйти</a></div>
     </header>-->

     <ul class="menu-main">
          <li><a href="" class="current" ;>Главная</a></li>
          <li><a href="/tours">Все туры</a></li>
          <li><a href="/orders">Заказы</a></li>
          <li><a href="/about">О нас</a></li>
          <li><a href="/logout">Выход</a></li>

     </ul>

     <section class="ticket">
		<div class="container">
			<div class="form">
				<h2>Подобрать тур</h2>
				<form action="/searchtour" method="post">
					<div class="form-block">

						Страна: <br>
                        <select class="select-input" name="tour">
                        <c:forEach var="tour" items="${tour}">

                        <option value="${tour.name}">"${tour.name}"</option>

                        </c:forEach>
                        </select>

					<br>
					<input type="submit" class="btn" value="Подобрать" />
				</form>
			</div>
		</div>
	</section>
<!--
     <div id="ram"></div>
     <a href="/reg.html" target="_parent"><input type="button" class="button4" value="Заказать"></a>
!-->
     </div>
	<div class="footer"> &copy;Туристическая компания "TOURS" &nbsp;<span class="separator">|</span>&nbsp; <a href="http://tours.ru">TOURS</a> </div>
	</div>
</body>
<script language="JavaScript" type="text/javascript" src="css/js/jquery-3.3.1.min.js"></script>
<!--<script language="JavaScript" type="text/javascript" src="css/js/login.js"></script>!-->

</html>
package com.epam.project.MyProject.mydto;

import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDate;

public class Tour {

    private int id;
    private String name;
    private String description;
    private int price ;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    public Tour(){};

    public Tour(int id, String name, String description, int price, LocalDate date){

        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.date = date;

    }

    public Tour(String name, String description, int price, LocalDate date){

        this.name = name;
        this.description = description;
        this.price = price;
        this.date = date;

    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getData() {
        return date;
    }

    public void setData(LocalDate date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Tour{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", date=" + date +
                '}';
    }
}

<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>

<head>

     <meta charset="UTF-8">
     <title>Турагенство</title>
     <link rel="stylesheet" href="css/index.css" media="screen" type="text/css" />
</head>

<body>

<ul class="menu-main">
          <li><a href="/home" class="current" ;>Главная</a></li>
          <li><a href="/tours">Все туры</a></li>
          <li><a href="/orders">Заказы</a></li>
          <li><a href="/about">О нас</a></li>
          <li><a href="/logout">Выход</a></li>

     </ul>
     <div id="ram">
     <h1>Все заказы</h1>
     <table>
     <tr>

          				<td><p> "Пользователь"</p></td>
          				<td><p> "Тур"</p></td>
          				<td><p> "Дата тура"</p></td>
          				<td><p> "Цена"</p></td>
          				<td><p> "Цена со скидкой"</p></td>
          			</tr>

     		<c:forEach var="order" items="${orders}">
     			<tr>
     				<!--<td><a href="/orders/${order.id}">${order.id_users}</a></td> -->
     				<td><p> ${order.username}</p></td>
     				<td><p> ${order.name}</p></td>
     				<td><p> ${order.date}</p></td>
     				<td><p> ${order.price}</p></td>
     				<td><p> ${order.discount}</p></td>
     			</tr>
     		</c:forEach>
     	</table>
         </div>

<div class="footer"> &copy;Туристическая компания "TOURS" &nbsp;<span class="separator">|</span>&nbsp; <a href="http://tours.ru">TOURS</a> </div>
${errorMsg}
</body>

</html>
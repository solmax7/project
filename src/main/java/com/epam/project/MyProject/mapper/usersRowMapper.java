package com.epam.project.MyProject.mapper;

import com.epam.project.MyProject.mydto.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;


import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class usersRowMapper implements RowMapper {


    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User users = new User(resultSet.getInt("id"),
                resultSet.getString("username"), resultSet.getString("login"), resultSet.getString("password"), resultSet.getBoolean("role"));
        return users;
    }
    }

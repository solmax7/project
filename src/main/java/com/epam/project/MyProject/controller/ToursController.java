package com.epam.project.MyProject.controller;

import com.epam.project.MyProject.exception.UnauthorizedException;
import com.epam.project.MyProject.mydto.Tour;
import com.epam.project.MyProject.mydto.User;
import com.epam.project.MyProject.service.TourService;
import com.epam.project.MyProject.service.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Controller

public class ToursController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private UserManager sessionUserManager;

    @Autowired
    private TourService tourManager;

    @GetMapping("/tours")
    public ModelAndView ViewAllTours(HttpServletRequest request) throws Exception{

        List<Tour> tours;

        ModelAndView modelAndView = new ModelAndView();
        tours = tourManager.ShowAllTours();

        List list = new ArrayList();
        List discount = new ArrayList();

        for (int i = 0; i < tours.size() ; i++) {

            int raz = (int) ChronoUnit.DAYS.between(LocalDate.now(), tours.get(i).getData());

            if (raz < 14) {

                list.add("ДА");
                discount.add(Math.round(tours.get(i).getPrice() * 0.3));
            } else {

                list.add("НЕТ");
                discount.add(0);
            }
        }

        modelAndView.addObject("tours", tours);
        modelAndView.addObject("list", list);
        modelAndView.addObject("discount", discount);
        modelAndView.setViewName("tours-page");
        return modelAndView;
    }

   @GetMapping(value = "/tour/{id}")
   private ModelAndView tours(@PathVariable int id, ModelAndView modelAndView) {
        Tour tour = tourManager.findTourById(id);
        User user = sessionUserManager.getUser();
        modelAndView.addObject("tour", tour);
        modelAndView.setViewName("tour-description");
        return modelAndView;
    }

    @PostMapping("/addTour")
    public ModelAndView addTour(ModelAndView modelAndView,Tour tour) throws Exception{

        tourManager.addNewTour(tour);

        modelAndView.setViewName("redirect:/admin");
        return modelAndView;
    }

    @PostMapping("/searchtour")
    public ModelAndView SearchTour(String tour) throws UnauthorizedException {

        List<Tour> tours;

        ModelAndView modelAndView = new ModelAndView();

        tours = tourManager.ShowAllToursByName(tour);

        List list = new ArrayList();
        List discount = new ArrayList();

        for (int i = 0; i < tours.size() ; i++) {

            int raz = (int) ChronoUnit.DAYS.between( LocalDate.now(),tours.get(i).getData() );

            if (raz < 14) {

                list.add("ДА");
                discount.add(Math.round(tours.get(i).getPrice() * 0.3));
            }
            else {

                list.add("НЕТ");
                discount.add(0);
            }
        }

        modelAndView.addObject("tours", tours);
        modelAndView.addObject("list", list);
        modelAndView.addObject("discount", discount);
        modelAndView.setViewName("tours-page");
        return modelAndView;
    }

}

package com.epam.project.MyProject.repository;

import com.epam.project.MyProject.exception.NotFoundException;
import com.epam.project.MyProject.exception.UnauthorizedException;
import com.epam.project.MyProject.mapper.usersRowMapper;
import com.epam.project.MyProject.mydto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public class usersSql extends NotFoundException {

        @Autowired
        JdbcTemplate jdbcTemplate;

        @Autowired

        usersRowMapper Row;

        public List<User> getByUsers() {
            String sql = "SELECT * FROM user";
            return  jdbcTemplate.query(sql, Row::mapRow);
        }

        public void addUser(User user) {
            String sql = "INSERT INTO user (username, login, password) " +
                    "VALUES (?, ?, ?)";
            jdbcTemplate.update(sql, user.getName(), user.getLogin(), user.getPassword());
        }

    public List<User> getByUserByLogin(String login) {
        String sql = "SELECT * FROM user where user.login = ?";
        return jdbcTemplate.query(sql, new Object[]{login}, Row::mapRow);
    }

    public List<User> FindUser(User user) throws UnauthorizedException {

        String sql = "SELECT * FROM user where user.login = ? and user.password = ?";

        return jdbcTemplate.query(sql, new Object[]{user.getLogin(), user.getPassword()}, Row::mapRow);

    }

    public String FindUserObj(User user) throws UnauthorizedException {

        String sql = "SELECT * FROM user where user.login = ? and user.password = ?";

        return jdbcTemplate.queryForObject(sql, new Object[]{user.getLogin(), user.getPassword()}, String.class);

    }


}




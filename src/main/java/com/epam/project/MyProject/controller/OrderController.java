package com.epam.project.MyProject.controller;

import com.epam.project.MyProject.exception.NotFoundException;
import com.epam.project.MyProject.mydto.Order;
import com.epam.project.MyProject.mydto.Tour;
import com.epam.project.MyProject.mydto.User;
import com.epam.project.MyProject.service.OrderService;
import com.epam.project.MyProject.service.TourService;
import com.epam.project.MyProject.service.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Controller
public class OrderController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private UserManager sessionUserManager;

    @Autowired
    private OrderService orderManager;

    @Autowired
    private TourService tourManager;


    @GetMapping("/addOrder")
    public ModelAndView Order(int tour, int user, int price) throws Exception{

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("tour", tour);
        return modelAndView;
    }

    @RequestMapping("/tour/{id}")
    public ModelAndView OrderBuy(@PathVariable int id, ModelAndView modelAndView) throws NotFoundException{

        Tour tour = tourManager.findTourById(id);

        LocalDate dayToday = LocalDate.now();
        int raz = (int) ChronoUnit.DAYS.between(tour.getData() , dayToday);

        if (raz < 14){

            orderManager.addOrder(id, sessionUserManager.getUser().getId(), tour.getPrice(), tour.getData(), 1, (int) Math.round(tour.getPrice() - (tour.getPrice() * 0.3)));

        }
        else {

            orderManager.addOrder(id, sessionUserManager.getUser().getId(), tour.getPrice(), tour.getData(), 0, tour.getPrice());

        }
       modelAndView.setViewName("redirect:/tours");
        return modelAndView;
    }

    @RequestMapping("/orders")
    public ModelAndView ViewAllTours(HttpServletRequest request) throws NotFoundException {

        List<Order> orders;

        ModelAndView modelAndView = new ModelAndView();
        User currentUser = sessionUserManager.getUser();

        try {

            if (currentUser.getRole()){

              orders = orderManager.ShowAllOrders();
              modelAndView.addObject("orders", orders);

            }
            else {
                orders = orderManager.ShowAllOrdersUsers(currentUser.getId());
                modelAndView.addObject("orders", orders);
            }
        }
        catch (Exception ex){

            modelAndView.addObject("errorMsg", ex.getMessage());
        }

        modelAndView.setViewName("orders-page");
        return modelAndView;
    }
}

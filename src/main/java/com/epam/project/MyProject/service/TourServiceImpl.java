package com.epam.project.MyProject.service;

import com.epam.project.MyProject.mydto.Tour;
import com.epam.project.MyProject.repository.tourSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class TourServiceImpl implements TourService {

    @Autowired
    tourSql tourQuery;

    @Override
    public void addNewTour(Tour tour) {

        tourQuery.addTour(tour);

    }

    @Override
    public List<Tour> findToursByUserLogin(String name) {

        List<Tour> ftour = tourQuery.getByTourByName(name);
        return ftour;
    }

    @Override
    public Tour findTourById(int id) {

        List<Tour> ftour = tourQuery.getByTourById(id);

        return ftour.get(0);
    }

    @Override
    public List<Tour>  ShowAllTours() {

        List <Tour> tours = tourQuery.getByTours();

        return tours;
    }

    @Override
    public List<Tour> ShowAllToursByName(String name) {

        List <Tour> tours = tourQuery.getByToursName(name);

        return tours;
    }
}
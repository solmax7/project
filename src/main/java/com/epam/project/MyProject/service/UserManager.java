package com.epam.project.MyProject.service;

import com.epam.project.MyProject.exception.UnauthorizedException;
import com.epam.project.MyProject.exception.UserAlreadyExistException;
import com.epam.project.MyProject.mydto.User;
import java.util.List;

public interface UserManager {

    User getUser();

     void setUser(User user);

     List<User> GetUserByLogin(String login);

     void addUserRegistration(User user) throws UserAlreadyExistException;

     List<User> FindUser(User user) throws UnauthorizedException;
}

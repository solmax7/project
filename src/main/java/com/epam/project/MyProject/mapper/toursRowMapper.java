package com.epam.project.MyProject.mapper;

import com.epam.project.MyProject.mydto.Tour;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;


@Component
public class toursRowMapper implements RowMapper {

    public Tour mapRow(ResultSet resultSet, int i) throws SQLException {
        Tour tour = new Tour(resultSet.getInt("id"),
                resultSet.getString("name"), resultSet.getString("description"), resultSet.getInt("price"),  resultSet.getDate("date").toLocalDate());
        return tour;
    }
}

$(document).ready(function () {
    $("#form-login").submit(
        function () {
                var user = document.getElementById("user").value;

                if(user.length < 4 || user.length > 20)
                    { alert('В логине должен быть от 4 до 20 символов'); return false;}
                if(parseInt(user.substr(0, 1)))
                    {alert('Логин должен начинаться с буквы'); return false;}

                 return true;

                });
        });


package com.epam.project.MyProject.service;

import com.epam.project.MyProject.exception.UnauthorizedException;
import com.epam.project.MyProject.exception.UserAlreadyExistException;
import com.epam.project.MyProject.mydto.User;
import com.epam.project.MyProject.repository.usersSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.util.List;

@SessionScope
@Service
public class UserManagerImpl implements UserManager{

    @Autowired
    usersSql userQuery;

    private User user;

    @Override
    public User getUser() {
        return user;
    }
    @Override
    public void setUser(User user) {
        this.user = user;
    }
    @Override
    public List<User> GetUserByLogin(String login){

        return userQuery.getByUserByLogin(login);

    }
    @Override
    public  void addUserRegistration(User user) throws UserAlreadyExistException{

        userQuery.addUser(user);

    }
    @Override
    public  List<User> FindUser(User user) throws UnauthorizedException {

        List<User> fuser = userQuery.FindUser(user);

        if (fuser == null ) {
            throw new UnauthorizedException("Пользователь не авторизован");
        }

        return fuser;

    }
}


package com.epam.project.MyProject.controller;

import com.epam.project.MyProject.exception.NotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionHandlerControllerr {
    
    
    @ExceptionHandler(NotFoundException.class)
    public ModelAndView handleException(NotFoundException ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("errorMsg");
        modelAndView.addObject("errorMsg", ex.getMessage());
        return modelAndView;
    }
    
    
    @ExceptionHandler(Exception.class)
    public ModelAndView handleRuntimeException(Exception ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("errorMsg");
        modelAndView.addObject("errorMsg", ex.getMessage());
        return modelAndView;
    }

}

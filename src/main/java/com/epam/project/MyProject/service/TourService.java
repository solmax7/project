package com.epam.project.MyProject.service;

import com.epam.project.MyProject.mydto.Order;
import com.epam.project.MyProject.mydto.Tour;

import java.util.List;

public interface TourService {

     void addNewTour(Tour tour);

    List<Tour> findToursByUserLogin(String login);

    Tour findTourById(int TourId);

    List<Tour> ShowAllTours();

    List<Tour> ShowAllToursByName(String name);
}
package com.epam.project.MyProject.mydto;

public class User {

    private int id;
    private String username;
    private Boolean role;
    private String login;
    private String password;

    public User(){};

    public User(int id, String username, String login, String password, Boolean role){

        this.id = id;
        this.username = username;
        this.login = login;
        this.password = password;
        this.role = role;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getRole() {
        return role;
    }

    public String getLogin() {

        return login;

    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setRole(Boolean role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    public String getName() {
        return username;
    }

    public void setName(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString(){

        return "id: " + id + ".name: " + username + ".role: " + role;

    }
}

package com.epam.project.MyProject.repository;

import com.epam.project.MyProject.mapper.orderRowMapper;
import com.epam.project.MyProject.mydto.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;

@Repository
public class orderSql {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    orderRowMapper Row;

    public List<Order> getByOrders() {
        String sql = "SELECT tour.`user`.username,  tour.`tour`.name, tour.`order`.date, tour.`order`.price, tour.`order`.discount FROM tour.`order`, tour.`user`,tour.`tour` Where tour.`order`.id_users = tour.`user`.id and tour.`order`.id_tour = tour.`tour`.id";
        return  jdbcTemplate.query(sql, Row::mapRowUser);
    }

    public void addOrder(int tour, int user, int price, LocalDate date, int firetour, int discount) {
        String sql = "INSERT INTO tour.`order` (id_tour, id_users, price, date, firetour, discount) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, tour, user, price, date, firetour, discount);
    }

    public List<Order> getByUserOrders(int id_users) {
        String sql = "SELECT tour.`user`.username,  tour.`tour`.name, tour.`order`.date, tour.`order`.price, tour.`order`.discount FROM tour.`order`, tour.`user`,tour.`tour` Where tour.`order`.id_users = tour.`user`.id and tour.`order`.id_tour = tour.`tour`.id and `order`.id_users = ?";
        return  jdbcTemplate.query(sql, new Object[]{id_users}, Row::mapRowUser);
    }


}
